cmake_minimum_required(VERSION 3.10)

project(lab1_task2)

set(CMAKE_CXX_FLAGS "-std=c++17 -pthread -O3 -pedantic -Wall -Werror")

set(${CMAKE_C_FLAGS}, "${CMAKE_C_FLAGS}")

set(CMAKE_BUILD_TYPE Debug)

add_executable(task3 main.cpp buffer.cpp)