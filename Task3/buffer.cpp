#include "buffer.h"

void Buffer::read_buffer()
{
    if(states==BufferStates::IDLE)
    {
        const std::lock_guard<std::mutex> lock(b_mutex);
        states = BufferStates::READING;

        if(buffer.empty())
        {
            std::cout << "Buffer is empty" << '\n';
            states = BufferStates::IDLE;
            return;
        }
        std::cout << buffer.front() << '\n';
        buffer.pop();
        states = BufferStates::IDLE;
    }else if(states==BufferStates::WRITING)
    {
        std::cout << "Writing in progress" << '\n';
    }else if(states==BufferStates::READING)
    {
        std::cout << "Reading in progress" << '\n';
    }
}

void Buffer::write_buffer(const char symbol)
{
    if(states==BufferStates::IDLE)
    {
        const std::lock_guard<std::mutex> lock(b_mutex);
        states=BufferStates::WRITING;
        if(buffer.size()>20)
        {
            std::cout << "Queue is full!" << '\n';
            states = BufferStates::IDLE;
            return;
        }
        buffer.push(symbol);
        states = BufferStates::IDLE;
    }else if(states==BufferStates::WRITING)
    {
        std::cout << "Writing in progress" << '\n';
    }else if(states==BufferStates::READING)
    {
        std::cout << "Reading in progress" << '\n';
    }
}