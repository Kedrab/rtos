#include "buffer.h"

using namespace std;

int main()
{
    Buffer test;
    thread t1(&Buffer::write_buffer, &test, 'a');
    thread t2(&Buffer::write_buffer, &test, 'b');

    thread t3(&Buffer::read_buffer, &test);
    thread t4(&Buffer::read_buffer, &test);

    t1.join();
    t2.join();
    t3.join();
    t4.join();

    return 0;
}