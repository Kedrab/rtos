#include <iostream>
#include <thread>
#include <mutex>
#include <queue>

class Buffer{
private:
    std::queue <char>buffer;
    std::mutex b_mutex;

    enum class BufferStates{
        READING,
        WRITING,
        IDLE
    }states;
public:
    void read_buffer();
    void write_buffer(const char symbol);
};