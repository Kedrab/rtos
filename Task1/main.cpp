#include <thread>
#include <iostream>
#include <mutex>
#include <chrono>

using namespace std;

int counter = 0;
mutex counter_mutex;

void just_add(const int a)
{
    this_thread::sleep_for(chrono::milliseconds(500));
    counter += a;
    cout << "Thread ID:" << this_thread::get_id() << "  counter value:" << counter << endl;
}

void mutex_add(const int a)
{
    const lock_guard<mutex> lock(counter_mutex);
    this_thread::sleep_for(chrono::milliseconds(500));
    counter += a;
    cout << "Thread ID:" << this_thread::get_id() << "  counter value:" << counter << endl;
}

int main()
{
    const int i=2;

    thread t1(just_add, i);
    thread t2(just_add, i);
    thread t3(just_add, i);
    
    if(t1.joinable())t1.join();
    if(t2.joinable())t2.join();
    if(t3.joinable())t3.join();

    counter = 0;
    cout << "Now testing mutex version!" << endl;


    thread t4(mutex_add, i);
    thread t5(mutex_add, i);
    thread t6(mutex_add, i);
    
    if(t4.joinable())t4.join();
    if(t5.joinable())t5.join();
    if(t6.joinable())t6.join();
    counter = 0;

    return 0;
}